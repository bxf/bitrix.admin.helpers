<?php

namespace BitFactory;
class AdminHelpers
{
    public static function OnBeforeProlog()
    {
        if (\CSite::InDir('/bitrix/admin/iblock_edit.php')) {
            \CJSCore::Init(array('jquery', 'translit'));
            /**@global \CMain $APPLICATION */
            global $APPLICATION;
            $strPathToModuleDir = str_replace($_SERVER['DOCUMENT_ROOT'], '', dirname(__FILE__));
            $APPLICATION->AddHeadScript($strPathToModuleDir . '/iblock_edit.js');
            $APPLICATION->SetAdditionalCSS($strPathToModuleDir . '/iblock_edit.css');
        }
    }
}