BX.ready(function () {
    if (document.location.toString().indexOf('/bitrix/admin/iblock_edit.php') !== -1) {
        var $properties_table = $('#edit2_edit_table');
        if ($properties_table.length) {
            $properties_table.find('input[name$=_CODE]').each(function () {
                $(this).parent().append('<img src="/bitrix/themes/.default/icons/iblock/link.gif" class="generate_property_code" title="Сгенериовать код из названия" alt="[X]" />');
            });
            $properties_table.on(
                'click',
                '.generate_property_code',
                function () {
                    var $tr = $(this).parent().parent();
                    var $input_name = $tr.find('input[name$=_NAME]');
                    var $input_code = $tr.find('input[name$=_CODE]');
                    var val = $.trim($input_name.val().toString());
                    if (val.length)
                        BX.translit(val, {
                            'max_len': 128,
                            'use_google': true,
                            'callback': function (result) {
                                $input_code.val(result.toString().toLocaleUpperCase().replace(/ /g, '_'));
                            }
                        });
                }
            );
        }
        var $iblock_params = $('#edit1'),
            $input_iblock_name = $iblock_params.find('input[name=NAME]'),
            $input_iblock_code = $iblock_params.find('input[name=CODE]');
        if ($iblock_params.length && $input_iblock_name.length && $input_iblock_code.length) {
            $input_iblock_code.parent().append('<img src="/bitrix/themes/.default/icons/iblock/link.gif" class="generate_iblock_code" title="Сгенериовать код из названия" alt="[X]" />');
            $iblock_params.on(
                'click',
                '.generate_iblock_code',
                function () {
                    var val = $.trim($input_iblock_name.val().toString());
                    if (val.length)
                        BX.translit(val, {
                            'max_len': 128,
                            'use_google': true,
                            'callback': function (result) {
                                $input_iblock_code.val(result.toString().toLocaleUpperCase().replace(/ /g, '_'));
                            }
                        });
                }
            );
        }
        var $iblock_seo = $('#edit10');
        if ($iblock_seo.length) {
            $iblock_seo.find('.adm-detail-title').append('<img src="/bitrix/themes/.default/icons/main/mnu_settings.gif" class="iblock_seo_set_default" title="Заполнить значениями по-умолчанию" alt="[auto]" />');
            $iblock_seo.on(
                'click',
                '.iblock_seo_set_default',
                function () {
                    $iblock_seo.find('textarea[id$=_TITLE]').val('{=this.Name}').change();
                    $iblock_seo.find('textarea[id$=_FILE_ALT]').val('{=this.Name}').change();
                    $iblock_seo.find('textarea[id$=_FILE_NAME]').val('{=this.Name}').change();
                    $iblock_seo.find('textarea[id$=_META_DESCRIPTION]').val('{=this.PreviewText}').change();
                    $iblock_seo.find('input:checkbox[id^=translit_]').attr('checked', true).change();
                    $iblock_seo.find('input:checkbox[id^=lower_]').attr('checked', true).change();
                    $iblock_seo.find('input:text[id^="space_"]').val('_').change();
                }
            );
        }
    }
});