# Дополнения административной панели Битрикс
## Установка с помощью Composer
Объявляем зависимость:

```
#!bash

composer require bitfactory/bitrix.admin.helpers dev-master
```

И дополнительно прописываем параметры:
```
#!json

{
    ...
    "repositories": [
        {
            "type": "vcs",
            "url": "git@bitbucket.org:bxf/bitrix.admin.helpers.git"
        }
    ]
}
```