<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    return false;
}

$manager = \Bitrix\Main\EventManager::getInstance();
$manager->addEventHandler('main', 'OnBeforeProlog', Array('\BitFactory\AdminHelpers', 'OnBeforeProlog'));
